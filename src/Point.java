public class Point 
{
	//w/o access modifier these fields are set to package
	//double x; // x coordinate ( a Point "has-a" x-coordinate )
	//double y; // y coordinate ( a Point "has-a" y-coordinate )
	
	private double x;
	private double y;
	
	// two-argument constructor
	public Point(double x, double y)
	{
		this.x = x; 
		this.y = y;
	} 
	
	public double getX()
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	// return string representation of Point object
	@Override
	public String toString()
	{
		return String.format("(%.1f, %.1f)", x, y);
	}
} // end class Point