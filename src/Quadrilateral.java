public class Quadrilateral 
{
	//these fields have package access
	/*
	Point point1; // first endpoint ( a quad "has-a point1 )
	Point point2; // second endpoint ( a quad "has-a point2 )
	Point point3; // third endpoint ( a quad "has-a point3 )
	Point point4; // fourth endpoint ( a quad "has-a point4 )
	*/
	
	/*
	 * Protected is an intermediate level of access b/t public and private
	 * if a field (or method) is protected
	 * 
	 * To ensure proper encapsulation and hiding of implementation details,
	 * we declare our fields as private, which means that we need to define
	 * public ,ethods o access or modify them
	 */
	private Point point1;
	private Point point2;
	private Point point3;
	private Point point4;
	
	// eight-argument constructor
	public Quadrilateral(double x1, double y1, double x2, double y2,
			double x3, double y3, double x4, double y4)
	{
		point1 = new Point(x1, y1);
		point2 = new Point(x2, y2);
		point3 = new Point(x3, y3);
		point4 = new Point(x4, y4);
	} 

	/**
	 * @return the point1
	 */
	public Point getPoint1() {
		return point1;
	}

	/**
	 * @return the point2
	 */
	public Point getPoint2() {
		return point2;
	}

	/**
	 * @return the point3
	 */
	public Point getPoint3() {
		return point3;
	}

	/**
	 * @return the point4
	 */
	public Point getPoint4() {
		return point4;
	}

	// return string representation of a Quadrilateral object
	// (Note the use of @Override annotation!)
	@Override
	public String toString()
	{
		return String.format("%s:\n%s", 
				"Coordinates of Quadrilateral are", getCoordinatesAsString()); 
	} 

	// return string containing coordinates as strings
	public String getCoordinatesAsString()
	{
		return String.format(
				"%s, %s, %s, %s\n", point1, point2, point3, point4);
	}
	
	public String getName()
	{
		return "Quadrilateral";
	}
} // end class Quadrilateral
