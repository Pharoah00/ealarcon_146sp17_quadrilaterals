public class Square extends Rectangle 
{
	// constructor
	public Square(double x1, double y1, double x2, double y2,
			double x3, double y3, double x4, double y4)
	{
		super(x1, y1, x2, y2, x3, y3, x4, y4);
		
		/*
		 * now that er've called the superclass constructor,
		 * we could perofrm some additionsl tasks or imnitialiazations
		 * that are specific tp a sqare object
		 */
		 //for example, to compute the distance between point 1 and point 2:
		
		double distance1to2 = Math.sqrt( Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2) );
		 
	} 
	

	// return string representation of Square object
	
	@Override
	public String toString()
	{
		return String.format("\n%s:\n%s%s: %s\n%s: %s\n",
				"Coordinates of " + getName() + " are",  getCoordinatesAsString(), 
				"Side is", getHeight(), "Area is", getArea());
	} 
	
	@Override
	public String getName()
	{
		return "Square,m \nwhich is also a " + super.getName();
	}
} // end class Square